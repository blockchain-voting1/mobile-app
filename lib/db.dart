import 'package:sqflite/sqflite.dart';

class DB {
  static Database _db;
  static int get _version => 1;
  static Database get instance => _db;

  static Future<void> init() async {
    if (_db != null) {
      return;
    }
    String _path = await getDatabasesPath() + 'wahl';
    _db = await openDatabase(
      _path,
      version: _version,
      onCreate: onCreate
    );
  }

  static void onCreate(Database db, int version) async {
    await db.execute("""CREATE TABLE voter (
      voterId INTEGER PRIMARY KEY NOT NULL,
      registrarId INTEGER,
      name VARCHAR(255) NOT NULL,
      email VARCHAR(255) NOT NULL,
      password VARCHAR(255) NOT NULL
    )""");

    await db.execute("""CREATE TABLE voter_identity (
      id INTEGER PRIMARY KEY NOT NULL,
      voterId INTEGER NOT NULL,
      type VARCHAR(255) NOT NULL,
      mspId VARCHAR(255) NOT NULL,
      certificate VARCHAR(255) NOT NULL,
      privateKey VARCHAR(255) NOT NULL,
      FOREIGN KEY (voterId) REFERENCES voter (voterId)
    )""");

    await db.execute("""CREATE TABLE vote (
      id INTEGER PRIMARY KEY NOT NULL,
      voterId INTEGER NOT NULL,
      electionId INTEGER NOT NULL,
      FOREIGN KEY (voterId) REFERENCES voter (voterId)
    )""");
  }
}