import 'package:shared_preferences/shared_preferences.dart';
import 'package:wahl/models/identity.dart';
import 'package:wahl/models/voter.dart';
import 'package:wahl/repositories/identity_repository.dart';
import 'package:wahl/repositories/voter_repository.dart';
import 'package:wahl/service/voters_api.dart';

class Voters {
  final VotersApi _votersApi;
  final VoterRepository _voterRepository;
  final IdentityRepository _identityRepository;

  Voters(
    this._votersApi,
    this._voterRepository,
    this._identityRepository
  );

  factory Voters.createInstance() {
    return new Voters(
        new VotersApi(),
        new VoterRepository(),
        new IdentityRepository()
    );
  }

  Future<void> create(Voter voter) async {
    Identity wallet = await _votersApi.create(voter);
    await _voterRepository.create(voter);
    await _identityRepository.create(voter.voterId, wallet);
    final prefs = await SharedPreferences.getInstance();
    await prefs.setString('voterId', voter.voterId);

  }

  Future<Voter> findByPassword(String password) {
    return _voterRepository.findByPassword(password);
  }
}