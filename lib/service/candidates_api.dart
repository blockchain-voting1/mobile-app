import 'dart:async';
import 'package:http/http.dart' as http;
import 'package:wahl/config/constants.dart';

const baseUrl = "$baseApiURL/elections/";

class CandidatesApi {
  static Future getCandidates(String electionId) async {
    return http.get(baseUrl + electionId + "/votable-items");
  }
}