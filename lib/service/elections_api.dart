import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:wahl/config/constants.dart';
import 'package:wahl/models/vote.dart';

const baseUrl = "$baseApiURL/elections/";

class ElectionsApi {
  static Future getElections() async {
    return http.get(baseUrl);
  }

  Future castVote(Vote vote, String electionId) async {
    return http.post(baseUrl + electionId + "/vote",
        headers: <String, String>{
          'Content-Type': 'application/json',
        },
        body: jsonEncode(vote.toMap()));
  }

  static Future results(String electionId) async {
    return http.get(baseUrl + electionId + "/results");
  }
}
