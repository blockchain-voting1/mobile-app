import 'package:shared_preferences/shared_preferences.dart';
import 'package:wahl/models/identity.dart';
import 'package:wahl/models/vote.dart';
import 'package:wahl/repositories/identity_repository.dart';
import 'package:wahl/repositories/vote_repository.dart';
import 'package:wahl/service/elections_api.dart';

class Elections {
  final ElectionsApi _electionsApi;
  final IdentityRepository _identityRepository;
  final VoteRepository _voteRepository;

  Elections(this._electionsApi, this._identityRepository, this._voteRepository);

  factory Elections.createInstance() {
    return new Elections(
      new ElectionsApi(),
      new IdentityRepository(),
      new VoteRepository()
    );
  }

  Future<void> castVote(
    String voterId,
    String electionId, String votableId
  ) async {
    Identity identity = await _identityRepository.findById(voterId);
    await _electionsApi.castVote(
      new Vote(voterId: voterId, votableId: votableId, identity: identity),
      electionId
    );
    await _voteRepository.create(electionId, voterId);
  }

  Future<bool> voted(String electionId) async {
    final prefs = await SharedPreferences.getInstance();
    return await _voteRepository.voted(electionId, prefs.getString('voterId'));
  }
}
