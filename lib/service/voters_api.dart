import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:wahl/config/constants.dart';
import 'package:wahl/models/identity.dart';
import 'package:wahl/models/voter.dart';

const baseUrl = "$baseApiURL/voters";

class VotersApi {
  Future<Identity> create(Voter voter) async {
    final response = await http.post(
      baseUrl,
      headers: <String, String>{
        'Content-Type': 'application/json',
      },
      body: jsonEncode(voter.toMap())
    );

    dynamic decoded = json.decode(response.body);

    return Identity.fromMap(decoded['identity']);
  }
}