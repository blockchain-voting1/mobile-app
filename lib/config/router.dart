import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:wahl/screens/candidate_screen.dart';
import 'package:wahl/screens/candidates_screen.dart';
import 'package:wahl/screens/elections_screen.dart';
import 'package:wahl/screens/login_screen.dart';
import 'package:wahl/screens/partial_result_screen.dart';
import 'package:wahl/screens/register_screen.dart';
import 'package:wahl/screens/vote_confirmation_screen.dart';

import 'constants.dart';

class Router {
  static Route<dynamic> onGenerateRoute(RouteSettings settings) {
    switch (settings.name) {
      case loginRoute:
        return MaterialPageRoute(builder: (_) => LoginScreen());
      case registerRoute:
        return MaterialPageRoute(builder: (_) => RegisterScreen());
      case electionsRoute:
        return MaterialPageRoute(builder: (_) => ElectionsScreen());
      case candidatesRoute:
        return MaterialPageRoute(
            builder: (_) => CandidatesScreen(
                  election: settings.arguments,
                ));
      case candidateRoute:
        return MaterialPageRoute(
            builder: (_) => CandidateScreen(candidate: settings.arguments));
      case confirmVoteRoute:
        return MaterialPageRoute(
            builder: (_) =>
                VoteConfirmationScreen(candidate: settings.arguments));
      case partialResultsRoute:
        return MaterialPageRoute(
            builder: (_) => PartialResultScreen(
                  electionId: settings.arguments,
                ));
      default:
        return MaterialPageRoute(
            builder: (_) => Scaffold(
                  body: Center(
                    child: Text('No route defined for ${settings.name}'),
                  ),
                ));
    }
  }
}
