const String loginRoute = '/login';
const String registerRoute = '/register';
const String electionsRoute = '/elections';
const String candidatesRoute = '/candidates';
const String candidateRoute = '/candidate';
const String confirmVoteRoute = '/confirmVote';
const String partialResultsRoute = '/partialResults';

const String baseApiURL = 'https://intense-refuge-69019.herokuapp.com';