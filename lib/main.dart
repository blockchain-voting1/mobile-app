import 'package:flutter/material.dart';
import 'package:wahl/config/constants.dart';
import 'package:wahl/config/router.dart';

import 'db.dart';

void main() async {
  try {
    WidgetsFlutterBinding.ensureInitialized();
    await DB.init();
    runApp(MyApp());
  } catch (ex) {
    print(ex);
  }
}


class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Wahl App',
      theme: ThemeData(
        primaryColor: Colors.white,
        // This makes the visual density adapt to the platform that you run
        // the app on. For desktop platforms, the controls will be smaller and
        // closer together (more dense) than on mobile platforms.
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      initialRoute: loginRoute,
      onGenerateRoute: Router.onGenerateRoute,
    );
  }
}