import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';

class InputFormatters {
  static final cpf = new MaskTextInputFormatter(
      mask: '###.###.###-##',
      filter: { "#": RegExp(r'[0-9]') }
  );

  static final rg = new MaskTextInputFormatter(
    mask: '##########',
    filter: { "#": RegExp(r'[0-9]') }
  );
}