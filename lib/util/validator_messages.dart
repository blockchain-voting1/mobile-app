class ValidatorMessages {
  static final String required = 'O campo é de preenchimento obrigatório';
  static final String email = 'O campo possui um valor de e-mail inválido';

  static String minLength(int length) {
    return 'O campo deve ter no mínimo $length caracteres';
  }
}