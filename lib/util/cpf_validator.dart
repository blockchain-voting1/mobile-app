import 'package:cpfcnpj/cpfcnpj.dart';
import 'package:form_field_validator/form_field_validator.dart';

class CpfValidator extends TextFieldValidator {
  CpfValidator({
    String errorText = 'O campo possui um valor de CPF inválido'
  }) : super(errorText);

  @override
  bool isValid(String value) {
    return CPF.isValid(value);
  }

}