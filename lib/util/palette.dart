import 'package:flutter/material.dart';

class Palette {
  static const MaterialColor primary = MaterialColor(_primaryValue, <int, Color>{
    50: Color(0xFFECF6EE),
    100: Color(0xFFCEE7D6),
    200: Color(0xFFAED8BA),
    300: Color(0xFF8EC89E),
    400: Color(0xFF75BC8A),
    500: Color(_primaryValue),
    600: Color(0xFF55A96D),
    700: Color(0xFF4BA062),
    800: Color(0xFF419758),
    900: Color(0xFF308745),
  });
  static const int _primaryValue = 0xFF5DB075;

  static const MaterialColor accent = MaterialColor(_accentValue, <int, Color>{
    100: Color(0xFFCFFFDA),
    200: Color(_accentValue),
    400: Color(0xFF69FF8B),
    700: Color(0xFF50FF78),
  });
  static const int _accentValue = 0xFF9CFFB3;
}