import 'identity.dart';

class Voter {
  final String voterId;
  final String registrarId;
  final String name;
  final String email;
  final String password;
  Identity identity;

  Voter({
    this.voterId,
    this.registrarId,
    this.name,
    this.email,
    this.password,
    this.identity
  });

  Map<String, dynamic> toMap() {
    return <String,dynamic>{
      'voterId': voterId,
      'registrarId': registrarId,
      'name': name,
      'email': email,
      'password': password,
    };
  }

  factory Voter.fromMap(Map<String, dynamic> map) {
    return new Voter(
      voterId: map['voterId'].toString(),
      registrarId: map['registrarId'],
      name: map['name'],
      email: map['email'],
      password: map['password']
    );
  }
}