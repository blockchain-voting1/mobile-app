import 'identity.dart';

class Vote {
  final String voterId;
  final String votableId;
  Identity identity;

  Vote({this.voterId, this.votableId, this.identity});

  Map<String, dynamic> toMap() {
    return <String,dynamic>{
      'voterId': voterId,
      'votableId': votableId,
      'identity': identity.toMap()
    };
  }
}
