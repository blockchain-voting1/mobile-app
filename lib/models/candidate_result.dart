class CandidateResult {
  final String votableId;
  final String img;
  final String description;
  final String politicalParty;
  final int votes;
  final double percentage;

  CandidateResult(
      {this.votableId,
      this.img,
      this.description,
      this.politicalParty,
      this.votes,
      this.percentage});

  factory CandidateResult.fromJson(Map<String, dynamic> json) {
    return CandidateResult(
        votableId: json['votableId'],
        img: json['img'],
        description: json['description'],
        politicalParty: json['politicalParty'],
        votes: json['votes'],
        percentage: json['percentage']);
  }
}
