class Election {
  final String electionId;
  final String name;
  final String type;
  final DateTime startDate;
  final DateTime endDate;

  Election(
      {this.electionId, this.name, this.type, this.startDate, this.endDate});

  factory Election.fromJson(Map<String, dynamic> json) {
    return Election(
        electionId: json['electionId'],
        name: json['name'],
        type: json['type'],
        startDate: DateTime.parse(json['startDate']),
        endDate: DateTime.parse(json['endDate']));
  }
}
