class PoliticalParty {
  final id;
  final String name;
  final String img;

  PoliticalParty({this.id, this.name, this.img});

  factory PoliticalParty.fromJson(Map<String, dynamic> json) {
    if (json == null) {
      return null;
    }

    return PoliticalParty(
        name: json['name'],
        img: json['img']);
  }

}
