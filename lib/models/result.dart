import 'candidate_result.dart';

class Result {
  final String electionId;
  final String name;
  final String type;
  final DateTime startDate;
  final DateTime endDate;
  final List<CandidateResult> candidateResult;
  final int totalVotes;

  Result(
      {this.electionId,
      this.name,
      this.type,
      this.startDate,
      this.endDate,
      this.candidateResult,
      this.totalVotes});

  factory Result.fromJson(Map<String, dynamic> json) {
    return Result(
        electionId: json['electionId'],
        name: json['name'],
        type: json['type'],
        startDate: DateTime.parse(json['startDate']),
        endDate: DateTime.parse(json['endDate']),
        candidateResult: (json['candidates'] as List)
            .map((e) {
                  e['percentage'] = (e['votes'] / json['totalVotes']) * 100;
                  return CandidateResult.fromJson(e);
                })
            .toList(),
        totalVotes: json['totalVotes']);
  }
}
