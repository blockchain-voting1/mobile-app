import 'package:wahl/models/political_party.dart';

class Candidate {
  final String id;
  final String name;
  final int count;
  final PoliticalParty politicalParty;
  final Candidate vice;
  final String position;
  final String img;
  String electionId;

  Candidate({
    this.id,
    this.name,
    this.count,
    this.politicalParty,
    this.position,
    this.vice,
    this.img
  });

  factory Candidate.fromJson(Map<String, dynamic> json) {
    return Candidate(
        id: json['votableId'],
        name: json['description'],
        img: json['img'],
        politicalParty: PoliticalParty.fromJson(json['politicalParty']),
        position: json['position'],
        vice: Candidate.viceFromJson(json['vice']));
  }

  factory Candidate.viceFromJson(Map<String, dynamic> json) {
    return Candidate(
      name: json['name'],
      img: json['img'],
      position: json['position']
    );
  }
}