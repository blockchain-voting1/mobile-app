class Identity {
  final int id;
  final String type;
  final String mspId;
  final String certificate;
  final String privateKey;

  Identity({
    this.id,
    this.type,
    this.mspId,
    this.certificate,
    this.privateKey
  });

  factory Identity.fromMap(Map<String, dynamic> map) {
    return new Identity(
      type: map['type'],
      mspId: map['mspId'],
      certificate: map['certificate'],
      privateKey: map['privateKey']
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'type': type,
      'mspId': mspId,
      'certificate': certificate,
      'privateKey': privateKey
    };
  }
}