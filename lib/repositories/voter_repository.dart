import 'package:sqflite/sqflite.dart';
import 'package:wahl/models/voter.dart';
import '../db.dart';

class VoterRepository {
  String _table = 'voter';
  String _pk = 'id';

  Future<int> create(Voter voter) async {
    return await DB.instance.insert(_table, voter.toMap());
  }

  Future<Voter> findById(int id) async {
    List<Map> result = await DB.instance
        .query(
        _table,
        where: '$_pk = ?',
        whereArgs: [id]
    );

    if (result.length > 0) {
      return Voter.fromMap(result.first);
    }

    return null;
  }

  Future<Voter> findByPassword(String password) async {
    List<Map> result = await DB.instance
        .query(
        _table,
        where: 'password = ?',
        whereArgs: [password]
    );

    if (result.length > 0) {
      return Voter.fromMap(result.first);
    }

    return null;
  }
}