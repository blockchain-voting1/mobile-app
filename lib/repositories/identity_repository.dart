import 'package:wahl/models/identity.dart';
import '../db.dart';

class IdentityRepository {
  String _table = 'voter_identity';

  Future<int> create(String voterId, Identity data) async {
    Map<String, dynamic> map = data.toMap();
    map['voterId'] = voterId;
    return await DB.instance.insert(_table, map);
  }
  
  Future<Identity> findById(String voterId) async {
    List<Map> result = await DB.instance
        .query(
        _table,
        where: 'voterId = ?',
        whereArgs: [voterId]
    );

    if (result.length > 0) {
      return Identity.fromMap(result.first);
    }

    return null;
  }
}