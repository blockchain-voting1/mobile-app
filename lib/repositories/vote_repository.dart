import '../db.dart';

class VoteRepository {
  String _table = 'vote';

  Future<int> create(String electionId, String voterId) async {
    return await DB.instance.insert(_table, <String, dynamic>{
      'electionId': electionId,
      'voterId': voterId
    });
  }

  Future<bool> voted(String electionId, String voterId) async {
    List<Map> result = await DB.instance
        .query(
        _table,
        where: 'electionId = ? AND voterID = ?',
        whereArgs: [electionId, voterId]
    );

    return result.isNotEmpty;
  }
}