import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:wahl/config/constants.dart';
import 'package:wahl/models/candidate.dart';
import 'package:wahl/models/political_party.dart';
import 'package:wahl/service/elections.dart';
import 'package:wahl/util/palette.dart';
import 'package:wahl/widgets/primary_button.dart';

class CandidateScreen extends StatefulWidget {
  final Candidate candidate;

  const CandidateScreen({
    Key key,
    this.candidate
  }) : super(key: key);

  @override
  State createState() => new _CandidateScreenState();
}

class _CandidateScreenState extends State<CandidateScreen> {
  final electionsService = Elections.createInstance();

  @override
  Widget build(BuildContext context) {
    final Candidate _candidate = widget.candidate;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(
          'Detalhes',
          style: TextStyle(color: Colors.white),
        ),
        centerTitle: true,
        backgroundColor: Palette.primary,
        iconTheme: IconThemeData(
            color: Colors.white
        ),
      ),
      body: Column(
        children: <Widget>[
          Stack(
            children: <Widget>[
              Container(
                height: 125.0,
                color: Palette.primary,
              ),
              ListView(
                shrinkWrap: true,
                children: <Widget>[
                  SizedBox(height: 10.0),
                  Center(
                    child: CircleAvatar(
                      radius: 75.0,
                      backgroundImage: NetworkImage(_candidate.img),
                    ),
                  )
                ],
              )
            ],
          ),
          SizedBox(height: 10.0),
          Text(
            _candidate.name,
            style: TextStyle(
                color: Colors.black,
                fontSize: 24.0,
                fontWeight: FontWeight.w700),
          ),
          Text(
            _candidate.position,
            style: TextStyle(
                color: Color(0xff666666),
                fontSize: 16.0
            ),
          ),
          Divider(),
          _viceCandidateTile(_candidate.vice),
          Divider(),
          _politicalPartyTile(_candidate.politicalParty),
          Divider(),
          _confirmButton(_candidate.id, _candidate.electionId)
        ],
      ),
    );
  }

  Widget _viceCandidateTile(Candidate vice) {
    return Column(
      children: <Widget>[
        CircleAvatar(
          backgroundColor: Colors.transparent,
          backgroundImage: NetworkImage(vice.img),
          radius: 50,
        ),
        SizedBox(height: 8.0),
        Text(
          vice.name,
          style: TextStyle(
              color: Colors.black,
              fontSize: 24.0,
              fontWeight: FontWeight.w700
          ),
        ),
        Text(
          vice.position,
          style: TextStyle(
              color: Color(0xff666666),
              fontSize: 16.0
          ),
        ),
      ],
    );
  }

  Widget _politicalPartyTile(PoliticalParty politicalParty) {
    return Column(
      children: <Widget>[
        CircleAvatar(
          radius: 25.0,
          backgroundColor: Colors.transparent,
          backgroundImage: NetworkImage(politicalParty.img),
        ),
        SizedBox(height: 8.0),
        Text(
          politicalParty.name,
          style: TextStyle(
              color: Color(0xff666666),
              fontSize: 14.0
          ),
        )
      ],
    );
  }

  Widget _confirmButton(String votableId, String electionId) {
    return PrimaryButton(
      labelText: 'CONFIRMAR VOTO',
      onPressed: () => _submit(votableId, electionId),
    );
  }

  void _submit(String votableId, String electionId) async {
   await _performVote(votableId, electionId);

    Navigator.pushNamed(
        context,
        confirmVoteRoute,
        arguments: widget.candidate
    );
  }

  Future<void> _performVote(String votableId, String electionId) async {
    final prefs = await SharedPreferences.getInstance();

    return await electionsService.castVote(prefs.getString('voterId'), electionId, votableId);
  }
}