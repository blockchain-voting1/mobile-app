import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import 'package:wahl/config/constants.dart';
import 'package:wahl/models/candidate_result.dart';
import 'package:wahl/models/result.dart';
import 'package:wahl/service/elections_api.dart';
import 'package:wahl/util/palette.dart';

class PartialResultScreen extends StatefulWidget {
  final String electionId;

  const PartialResultScreen({Key key, this.electionId}) : super(key: key);

  @override
  State createState() => new _PartialResultScreenState();
}

class _PartialResultScreenState extends State<PartialResultScreen> {
  Result _result;
  List<CandidateResult> _candidates = List<CandidateResult>();

  @override
  void initState() {
    super.initState();

    ElectionsApi.results(widget.electionId).then((value) => {
      setState(() {
        _result = Result.fromJson(json.decode(value.body));
        _result.candidateResult.sort((a, b) => b.votes.compareTo(a.votes));
        _candidates = _result.candidateResult;
      })
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text('Parciais'),
        centerTitle: true,
        actions: <Widget>[
          Padding(
              padding: EdgeInsets.only(right: 20.0),
              child: GestureDetector(
                onTap: () {
                  Navigator.pushNamed(
                      context,
                      electionsRoute
                  );
                },
                child: Icon(
                    Icons.home
                ),
              )
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(
              height: 70,
              color: Colors.grey,
              child: Center(
                child: Text(
                  'EM PROGRESSO',
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.w700,
                    fontSize: 32.0
                  ),
                ),
              ),
            ),
            SizedBox(height: 20),
            ListView.separated(
              itemCount: _candidates.length,
              scrollDirection: Axis.vertical,
              shrinkWrap: true,
              separatorBuilder: (BuildContext context, index) => Divider(),
              itemBuilder: (BuildContext _context, int index) {
                return candidatePartial(_candidates[index]);
              },
            )
          ],
        ),
      ),
    );
  }

  Widget candidatePartial(CandidateResult candidate) {
    return ListTile(
      leading: CircleAvatar(
        radius: 32,
        backgroundImage: NetworkImage(candidate.img),
      ),
      title: Text(
        candidate.description,
        style: TextStyle(
          fontSize: 22,
          fontWeight: FontWeight.w700
        ),
      ),
      subtitle: ListView(
        shrinkWrap: true,
        children: <Widget>[
          Text(
            candidate.politicalParty,
            style: TextStyle(
                fontSize: 16
            ),
          ),
          SizedBox(height: 5),
          LinearPercentIndicator(
            linearStrokeCap: LinearStrokeCap.butt,
            progressColor: Palette.primary,
            padding: null,
            percent: (candidate.percentage / 100),
            lineHeight: 8,
            width: 170,
          )
        ],
      ),
      trailing: Column(
        children: <Widget>[
          Text(
            candidate.percentage.toStringAsFixed(2).replaceAll(RegExp(r"([.]*0)(?!.*\d)"), "") + '%',
            style: TextStyle(
                fontSize: 28,
                fontWeight: FontWeight.w700
            ),
          ),
          Text(
            candidate.votes.toString() + " votos",
            style: TextStyle(color: Colors.grey),
          )
        ],
      ),
    );
  }
}