import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:wahl/config/constants.dart';
import 'package:wahl/models/election.dart';
import 'package:wahl/service/elections.dart';
import 'package:wahl/service/elections_api.dart';
import 'package:wahl/util/palette.dart';
import 'package:wahl/widgets/search_field.dart';

class ElectionsScreen extends StatefulWidget {
  @override
  State createState() => new _ElectionsScreenState();
}

class _ElectionsScreenState extends State<ElectionsScreen> {
  List<Election> _elections = List<Election>();
  Elections _service = Elections.createInstance();
  bool _showFilters = false;

  @override
  void initState() {
    super.initState();

    ElectionsApi.getElections().then((value) => {
          setState(() {
            Iterable list = json.decode(value.body);
            _elections =
                list.map((e) => Election.fromJson(e['Record'])).toList();
          })
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: Text('Eleições'),
          centerTitle: true,
          automaticallyImplyLeading: false,
          actions: <Widget>[
            FlatButton(
              textColor: Palette.primary,
              child: Text('Filtrar'),
              onPressed: _toggleFilters,
            )
          ],
        ),
        body: Column(
          children: <Widget>[
            _showFilters
                ? Container(
                    padding:
                        EdgeInsets.only(left: 24.0, right: 24.0, top: 40.0),
                    child: SearchField())
                : Container(),
            ListView.builder(
                itemCount: _elections.length,
                scrollDirection: Axis.vertical,
                shrinkWrap: true,
                itemBuilder: (BuildContext _context, int index) {
                  if (index.isOdd) {
                    return Divider();
                  }
                  return ListTile(
                    title: Text(_elections[index].name.toUpperCase(),
                        style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                          backgroundColor: Color.fromRGBO(200, 200, 200, 0.3)
                        ),
                        textAlign: TextAlign.center,),

                    onTap: () => _tapElection(_elections[index]),
                  );
                }),
          ],
        ));
  }

  void _toggleFilters() {
    setState(() {
      _showFilters = !_showFilters;
    });
  }

  Future<void> _tapElection(Election election) async {
    if (await _service.voted(election.electionId)) {
      Navigator.pushNamed(context, partialResultsRoute,
          arguments: election.electionId);
      return;
    }

    Navigator.pushNamed(context, candidatesRoute, arguments: election);
  }
}
