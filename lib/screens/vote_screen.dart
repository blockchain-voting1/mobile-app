import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:wahl/models/candidate.dart';
import 'package:wahl/screens/vote_confirmation_screen.dart';
import 'package:wahl/service/elections.dart';

import 'candidate_screen.dart';

class VoteScreen extends StatefulWidget {
  final Candidate candidate;

  const VoteScreen({
    Key key,
    this.candidate
  }) : super(key: key);
  @override
  State createState() => new _VoteScreenState();
}

class _VoteScreenState extends State<VoteScreen> {
  bool voted = false;
  Elections _elections = Elections.createInstance();

  @override
  void initState() {
    super.initState();
    _elections.voted(widget.candidate.electionId).then((voted) => {
      setState(() {
        voted = voted;
      })
    });

  }

  @override
  Widget build(BuildContext context) {
    if (voted) {
      return VoteConfirmationScreen(candidate: widget.candidate);
    }

    return CandidateScreen(candidate: widget.candidate);
  }
}