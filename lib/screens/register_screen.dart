import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:wahl/config/constants.dart';
import 'package:wahl/models/voter.dart';
import 'package:wahl/service/voters.dart';
import 'package:wahl/service/voters_api.dart';
import 'package:wahl/util/cpf_validator.dart';
import 'package:wahl/util/input_fomatters.dart';
import 'package:wahl/util/validator_messages.dart';
import 'package:wahl/widgets/custom_text_form_field.dart';
import 'package:wahl/widgets/primary_button.dart';

class RegisterScreen extends StatefulWidget {

  @override
  State createState() => new _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  final formKey = new GlobalKey<FormState>();
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  final votersService = Voters.createInstance();
  bool _loading = false;
  String _registrarId;
  String _email;
  String _name;
  String _voterId;
  String _password;
  String _confirmPassword;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      backgroundColor: Colors.white,
      appBar: AppBar(
        centerTitle: true,
        title: Text('Cadastro')
      ),
      body: Center(
        child: Form(
          key: formKey,
            child: ListView(
              shrinkWrap: true,
              padding: EdgeInsets.only(left: 24.0, right: 24.0),
              children: <Widget>[
                _nameField(),
                _inputDivider(),
                _emailField(),
                _inputDivider(),
                _registrarIdField(),
                _inputDivider(),
                _inputDivider(),
                _voterIdField(),
                _inputDivider(),
                _passwordField(),
                _inputDivider(),
                _confirmPasswordField(),
                SizedBox(height: 24.0),
                _registerButton(),
              ],
            )
        ),
      )
    );
  }


  void _toggleLoading() {
    setState(() {
      _loading = !_loading;
    });
  }

  void _submit() {
    final form = formKey.currentState;
    if (form.validate()) {
      form.save();
      _performRegistration();
    }
  }

  void _performRegistration() async {
    final voter = new Voter(
        name: _name,
        email: _email,
        voterId: _voterId,
        registrarId: _registrarId,
        password: _password
    );
    _toggleLoading();
    try {
      await votersService.create(voter);
      _toggleLoading();
      scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: new Text("Usuário criado com sucesso!"),
      ));
      Navigator.pushNamed(context, electionsRoute);
    } catch (e) {
      _toggleLoading();
      scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: new Text(e.toString()),
      ));
    }
  }

  Widget _nameField() {
    return CustomTextFormField(
      labelText: 'Nome',
      validator: RequiredValidator(errorText: ValidatorMessages.required),
      onSaved: (name) => _name = name,
    );
  }

  Widget _registrarIdField() {
    return CustomTextFormField(
      labelText: 'CPF',
      inputFormatters: [InputFormatters.cpf],
      onSaved: (number) => _registrarId = number,
      validator: MultiValidator([
        RequiredValidator(errorText: ValidatorMessages.required),
        CpfValidator()
      ]),
    );
  }

  Widget _voterIdField() {
    return CustomTextFormField(
      labelText: 'Título Eleitoral',
      onSaved: (number) => _voterId = number,
      validator: MultiValidator([
        RequiredValidator(errorText: ValidatorMessages.required)
      ]),
    );
  }

  Widget _emailField() {
    return CustomTextFormField(
      labelText: 'E-mail',
      inputType: TextInputType.emailAddress,
      onSaved: (email) => _email = email,
      validator: MultiValidator([
        RequiredValidator(errorText: ValidatorMessages.required),
        EmailValidator(errorText: ValidatorMessages.email)
      ]),
    );
  }

  Widget _passwordField() {
    return CustomTextFormField(
      labelText: 'Senha',
      obscureText: true,
      onChanged: (password) => _password = password,
      validator: MultiValidator([
        RequiredValidator(errorText: ValidatorMessages.required),
        MinLengthValidator(6, errorText: ValidatorMessages.minLength(6))
      ]),
    );
  }

  Widget _confirmPasswordField() {
    return CustomTextFormField(
      labelText: 'Confirmar senha',
      obscureText: true,
      onSaved: (confirmPassword) => _confirmPassword = confirmPassword,
      validator: (val) => MatchValidator(
        errorText: 'O campo possui um valor diferente do campo senha'
      ).validateMatch(val, _password),
    );
  }

  Widget _inputDivider() {
    return const SizedBox(height: 8.0);

  }

  Widget _registerButton() {
    if (_loading) {
      return PrimaryButton(labelText: 'Carregando...');
    }
    return PrimaryButton(
      labelText: 'Cadastrar',
      onPressed:  _submit
    );
  }
}