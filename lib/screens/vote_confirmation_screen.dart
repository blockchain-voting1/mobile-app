import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:wahl/config/constants.dart';
import 'package:wahl/models/candidate.dart';
import 'package:wahl/models/political_party.dart';
import 'package:wahl/util/palette.dart';
import 'package:wahl/widgets/primary_button.dart';

class VoteConfirmationScreen extends StatefulWidget {
  final Candidate candidate;

  const VoteConfirmationScreen({Key key, this.candidate}) : super(key: key);

  @override
  State createState() => new _VoteConfirmationScreenState();
}

class _VoteConfirmationScreenState extends State<VoteConfirmationScreen> {
  @override
  Widget build(BuildContext context) {
    final Candidate _candidate = widget.candidate;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(
          'Confirmação',
          style: TextStyle(color: Colors.white),
        ),
        centerTitle: true,
        backgroundColor: Palette.primary,
        iconTheme: IconThemeData(color: Colors.white),
        automaticallyImplyLeading: false,
        actions: <Widget>[
          Padding(
              padding: EdgeInsets.only(right: 20.0),
              child: GestureDetector(
                onTap: () {
                  Navigator.pushNamed(
                      context,
                      electionsRoute
                  );
                },
                child: Icon(
                    Icons.home
                ),
              )
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Stack(
              children: <Widget>[
                Container(
                  height: 125.0,
                  color: Palette.primary,
                ),
                ListView(
                  shrinkWrap: true,
                  children: <Widget>[
                    SizedBox(height: 20.0),
                    Center(
                      child: Text(
                        'VOCÊ VOTOU EM',
                        style: TextStyle(
                            fontSize: 32.0,
                            color: Colors.white,
                            fontWeight: FontWeight.w700),
                      ),
                    ),
                    SizedBox(height: 10.0),
                    Center(
                      child: CircleAvatar(
                        radius: 60.0,
                        backgroundImage: NetworkImage(_candidate.img),
                      ),
                    )
                  ],
                )
              ],
            ),
            SizedBox(height: 10.0),
            Text(
              _candidate.name,
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 20.0,
                  fontWeight: FontWeight.w700),
            ),
            Text(
              _candidate.position,
              style: TextStyle(color: Color(0xff666666), fontSize: 16.0),
            ),
            Divider(),
            _viceCandidateTile(_candidate.vice),
            Divider(),
            _politicalPartyTile(_candidate.politicalParty),
            Divider(),
            _confirmButton()
          ],
        ),
      ),
    );
  }

  Widget _viceCandidateTile(Candidate vice) {
    return Column(
      children: <Widget>[
        CircleAvatar(
          backgroundColor: Colors.transparent,
          backgroundImage: NetworkImage(vice.img),
          radius: 50,
        ),
        SizedBox(height: 8.0),
        Text(
          vice.name,
          style: TextStyle(
              color: Colors.black, fontSize: 20.0, fontWeight: FontWeight.w700),
        ),
        Text(
          vice.position,
          style: TextStyle(color: Color(0xff666666), fontSize: 16.0),
        ),
      ],
    );
  }

  Widget _politicalPartyTile(PoliticalParty politicalParty) {
    return Column(
      children: <Widget>[
        CircleAvatar(
          radius: 25.0,
          backgroundColor: Colors.transparent,
          backgroundImage: NetworkImage(politicalParty.img),
        ),
        SizedBox(height: 8.0),
        Text(
          politicalParty.name,
          style: TextStyle(color: Color(0xff666666), fontSize: 14.0),
        )
      ],
    );
  }

  Widget _confirmButton() {
    return PrimaryButton(
      labelText: 'VER PARCIAIS',
      onPressed: () {
        Navigator.pushNamed(
          context,
          partialResultsRoute,
          arguments: widget.candidate.electionId
        );
      },
    );
  }
}
