import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:wahl/config/constants.dart';
import 'package:wahl/models/voter.dart';
import 'package:wahl/service/voters.dart';
import 'package:wahl/util/input_fomatters.dart';
import 'package:wahl/util/palette.dart';
import 'package:wahl/util/validator_messages.dart';
import 'package:wahl/widgets/custom_text_form_field.dart';
import 'package:wahl/widgets/primary_button.dart';

class LoginScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => new _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final formKey = new GlobalKey<FormState>();
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  final votersService = Voters.createInstance();
  bool _hidePassword = true;
  String _cpf;
  String _password;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      backgroundColor: Colors.white,
      appBar: AppBar(
        centerTitle: true,
        title: const Text('Login'),
        automaticallyImplyLeading: false,
      ),
      body: Center(
          child: Form(
              key: formKey,
              child: ListView(
                shrinkWrap: true,
                padding: EdgeInsets.only(left: 24.0, right: 24.0),
                children: <Widget>[
                  _wahlTitle(),
                  SizedBox(height: 40.0),
                  _passwordInput(),
                  SizedBox(height: 24.0),
                  _loginButton(),
                  _registerButton(),
                ],
              ))),
    );
  }

  void _toggleHidePassword() {
    setState(() {
      _hidePassword = !_hidePassword;
    });
  }

  void _submit() {
    final form = formKey.currentState;
    if (form.validate()) {
      form.save();
      _performLogin();
    }
  }

  void _performLogin() async {
    final Voter voter = await votersService.findByPassword(_password);
    final prefs = await SharedPreferences.getInstance();

    if (voter != null) {
      prefs.setString('voterId', voter.voterId);
      Navigator.pushNamed(context, electionsRoute);
      return;
    }

    final snackbar = new SnackBar(
      content:
          new Text("Senha inválida, verifique a senha digitada novamente!"),
    );

    scaffoldKey.currentState.showSnackBar(snackbar);
  }

  Widget _wahlTitle() {
    return Text('WAHL',
        textAlign: TextAlign.center,
        style: TextStyle(
            fontFamily: 'NovaFlat', color: Palette.primary, fontSize: 48));
  }

  Widget _cpfInput() {
    return CustomTextFormField(
        labelText: 'CPF',
        inputFormatters: [InputFormatters.cpf],
        onSaved: (cpf) => _cpf = cpf,
        validator: RequiredValidator(errorText: ValidatorMessages.required));
  }

  Widget _passwordInput() {
    return CustomTextFormField(
        labelText: 'Senha',
        obscureText: _hidePassword,
        onSaved: (password) => _password = password,
        validator: RequiredValidator(errorText: ValidatorMessages.required),
        suffixIcon: IconButton(
          icon: Icon(_hidePassword ? Icons.visibility : Icons.visibility_off,
              color: Palette.primary),
          onPressed: _toggleHidePassword,
        ));
  }

  Widget _loginButton() {
    return PrimaryButton(
      labelText: 'Login',
      onPressed: _submit,
    );
  }

  Widget _registerButton() {
    return FlatButton(
      child: Text(
        'Cadastrar',
        style: TextStyle(color: Palette.primary),
      ),
      onPressed: () {
        Navigator.pushNamed(context, registerRoute);
      },
    );
  }
}