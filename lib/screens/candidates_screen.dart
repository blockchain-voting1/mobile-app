import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:wahl/config/constants.dart';
import 'package:wahl/models/candidate.dart';
import 'package:wahl/models/election.dart';
import 'package:wahl/service/candidates_api.dart';
import 'package:wahl/util/palette.dart';
import 'package:wahl/widgets/candidate_card.dart';
import 'package:wahl/widgets/search_field.dart';

class CandidatesScreen extends StatefulWidget {
  final Election election;

  const CandidatesScreen({Key key, this.election}) : super(key: key);

  @override
  State createState() => new _CandidatesScreenState();
}

class _CandidatesScreenState extends State<CandidatesScreen> {
  bool _showFilters = false;
  List<Candidate> _candidates = List<Candidate>();

  @override
  void initState() {
    super.initState();

    CandidatesApi.getCandidates(widget.election.electionId).then((value) => {
          setState(() {
            Iterable list = json.decode(value.body);
            _candidates =
                list.map((e) => Candidate.fromJson(e['Record'])).toList();
          })
        });
  }

  @override
  Widget build(BuildContext context) {
    final Election _election = widget.election;
    return Scaffold(
      appBar: AppBar(
        title: Text('Votar'),
        centerTitle: true,
        actions: <Widget>[
          FlatButton(
            textColor: Palette.primary,
            child: Text('Filtrar'),
            onPressed: _toggleFilters,
          ),
          Padding(
              padding: EdgeInsets.only(right: 20.0),
              child: GestureDetector(
                onTap: () {
                  Navigator.pushNamed(context, electionsRoute);
                },
                child: Icon(Icons.home),
              )),
        ],
      ),
      body: Column(
        children: <Widget>[
          _showFilters
              ? Container(
                  color: Colors.white,
                  padding: EdgeInsets.only(
                      left: 24.0, right: 24.0, top: 24.0, bottom: 24.0),
                  child: SearchField())
              : Container(),
          Expanded(
              child: ListView.builder(
                  scrollDirection: Axis.vertical,
                  padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 8.0),
                  shrinkWrap: true,
                  itemCount: _candidates.length,
                  itemBuilder: (BuildContext context, int index) {
                    return CandidateCard(
                      candidate: _candidates[index],
                      election: _election,
                      onSelect: _selectCandidate,
                    );
                  }))
        ],
      ),
    );
  }

  void _toggleFilters() {
    setState(() {
      _showFilters = !_showFilters;
    });
  }

  void _selectCandidate(Candidate candidate, Election election) {
    candidate.electionId = election.electionId;
    Navigator.pushNamed(context, candidateRoute, arguments: candidate);
  }
}
