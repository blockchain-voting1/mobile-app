import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:wahl/models/candidate.dart';
import 'package:wahl/models/election.dart';
import 'package:wahl/widgets/primary_button.dart';

class CandidateCard extends StatelessWidget {
  final Candidate candidate;
  final Election election;
  final void Function(Candidate candidate, Election election) onSelect;

  const CandidateCard({
    Key key,
    @required this.candidate,
    @required this.election,
    this.onSelect
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Card(
          child: Center(
            child: Column(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.only(top: 8.0, bottom: 8.0),
                  child: CircleAvatar(
                    radius: 42.0,
                    backgroundColor: Colors.transparent,
                    backgroundImage: NetworkImage(candidate.img),
                  ),
                ),
                Text(
                  candidate.name,
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 16.0,
                      fontWeight: FontWeight.w700),
                ),
                Text(
                  candidate.position,
                  style: TextStyle(color: Color(0xff666666), fontSize: 14.0),
                ),
                SizedBox(height: 24.0),
                Text(
                  candidate.vice.name,
                  style: TextStyle(
                    fontWeight: FontWeight.w700,
                    fontSize: 14.0,
                  ),
                ),
                Text(
                  candidate.vice.position,
                  style: TextStyle(
                    color: Color(0xff666666),
                    fontSize: 14.0,
                  ),
                ),
                _selectButton(),
              ],
            ),
          ),
        ),
        Positioned(
          right: 24.0,
          top: 24.0,
          child: Column(
            children: <Widget>[
              CircleAvatar(
                radius: 20.0,
                backgroundColor: Colors.transparent,
                backgroundImage: NetworkImage(candidate.politicalParty.img),
              ),
              SizedBox(height: 8.0),
              Text(
                candidate.politicalParty.name,
                style: TextStyle(
                  color: Color(0xff666666),
                  fontSize: 10.0,
                ),
              )
            ],
          ),
        )
      ],
    );
  }

  Widget _selectButton() {
    return PrimaryButton(
      labelText: 'Selecionar',
      onPressed: () => onSelect(candidate, election),
    );
  }
}
