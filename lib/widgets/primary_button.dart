import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:wahl/util/palette.dart';

class PrimaryButton extends StatelessWidget {
  final VoidCallback onPressed;
  final String labelText;

  const PrimaryButton({
    Key key,
    @required this.labelText,
    this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 16.0),
      child: RaisedButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(24),
        ),
        padding: EdgeInsets.all(12),
        color: Palette.primary,
        child: Text(labelText, style: TextStyle(color: Colors.white)),
        onPressed: onPressed,
      )
    );
  }
}