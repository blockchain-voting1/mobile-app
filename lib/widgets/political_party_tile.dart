import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:wahl/models/political_party.dart';

class PoliticalPartyTile extends StatelessWidget {
  final PoliticalParty politicalParty;


  const PoliticalPartyTile({
    Key key,
    @required this.politicalParty
  }) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        CircleAvatar(
          radius: 25.0,
          backgroundColor: Colors.transparent,
          backgroundImage: NetworkImage(politicalParty.img),
        ),
        SizedBox(height: 8.0),
        Text(
          politicalParty.name,
          style: TextStyle(
            color: Color(0xff666666),
            fontSize: 14.0
          ),
        )
      ],
    );
  }
}