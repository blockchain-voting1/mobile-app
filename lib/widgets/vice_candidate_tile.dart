import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:wahl/models/candidate.dart';

class ViceCandidateTile extends StatelessWidget {
  final Candidate vice;

  const ViceCandidateTile({
    Key key,
    @required this.vice
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        CircleAvatar(
          backgroundColor: Colors.transparent,
          backgroundImage: NetworkImage(vice.img),
          radius: 50,
        ),
        SizedBox(height: 8.0),
        _nameTile(vice.name),
        _positionTile(vice.position),
      ],
    );
  }

  Widget _nameTile(String name) {
    return Text(
      name,
      style: TextStyle(
          color: Colors.black,
          fontSize: 24.0,
          fontWeight: FontWeight.w700),
    );
  }

  Widget _positionTile(String position) {
    return Text(
      position,
      style: TextStyle(
          color: Color(0xff666666),
          fontSize: 16.0
      ),
    );
  }
}