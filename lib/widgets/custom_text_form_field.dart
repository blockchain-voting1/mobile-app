import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class CustomTextFormField extends StatelessWidget {
  final List<TextInputFormatter> inputFormatters;
  final String labelText;
  final bool obscureText;
  final Widget suffixIcon;
  final TextInputType inputType;
  final FormFieldSetter<String> onSaved;
  final ValueChanged<String> onChanged;
  final FormFieldValidator<String> validator;

  const CustomTextFormField(
      {Key key,
      @required this.labelText,
      this.inputFormatters,
      this.obscureText = false,
      this.suffixIcon,
      this.inputType = TextInputType.text,
      this.onSaved,
      this.onChanged,
      this.validator})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      inputFormatters: inputFormatters,
      obscureText: obscureText,
      keyboardType: inputType,
      validator: validator,
      autofocus: false,
      onSaved: onSaved,
      onChanged: onChanged,
      decoration: InputDecoration(
        suffixIcon: suffixIcon,
        labelText: labelText,
        filled: true,
        fillColor: Color(0xfff6f6f6),
        labelStyle: TextStyle(color: Colors.grey),
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: _outlineBorder(),
        focusedBorder: _outlineBorder(),
      ),
    );
  }

  InputBorder _outlineBorder() {
    return OutlineInputBorder(borderRadius: BorderRadius.circular(8.0));
  }
}
