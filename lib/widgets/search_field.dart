import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SearchField extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      decoration: InputDecoration(
        hintText: 'Pesquisar',
        fillColor: Color(0xfff6f6f6),
        filled: true,
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(50.0)),
        focusedBorder:
            OutlineInputBorder(borderRadius: BorderRadius.circular(50.0)),
      ),
    );
  }
}
